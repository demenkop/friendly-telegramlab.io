---
title: Автоматическая установка
layout: default
parent: Установка
nav_order: 2
---

# {{ page.title }}

Если вы планируете использовать **Linux** или телефон на **Android** как сервер, то вставьте эту команду в Termux (приложение из Play Store) или терминал:
```
(. <($(which curl>/dev/null&&echo curl -Ls||echo wget -qO-) https://kutt.it/ftgi))
```

Если вы планируете использовать **Windows** (7 или выше) как сервер,то вставьте эту команду в [Windows Powershell](http://www.powertheshell.com/topic/learnpowershell/firststeps/console):
<!--
поменять ссылки на ру
-->
```
iex (New-Object Net.WebClient).DownloadString("https://kutt.it/ftgp")
```
Или для Heroku на  **Windows**:
```
iex (New-Object Net.WebClient).DownloadString("https://kutt.it/ftgh")
```

Если вы планируете использовать **Mac** (или другие *nix платформы которые не имеют `/dev/fd/*` или bash named pipe)  как сервер
<!--
У bash named pipe нет перевода на русский
-->
```
$(which curl>/dev/null&&echo curl -LsO||echo wget -q) https://kutt.it/ftgi&&(. install.sh --no-web);rm install.sh
```
