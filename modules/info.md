---
title: Системная информация
layout: default
parent: Modules
---

# {{ page.title }}

## Commands

- **Проверяет информацию о хосте/сервере.**
[Syntax: `info`]

  Возвращает базовую системную информацию о машине-хосте, на которой установлен юзербот.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzg0MTIzMTg0XX0=
-->
