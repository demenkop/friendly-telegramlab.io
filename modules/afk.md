---
title: AFK
layout: default
parent: Modules
---

# {{ page.title }}

## Commands

 - * Вход в режим   K**
[Syntax: `afk [причина (опционально)]`]

   Включает автоматическое оповещение о вашем отсутствии. Если не была указана причина, то и объяснение будет отсутствовать.

 - * Выход из режима AFK**
[Syntax: `unafk`]

   Выключает оповещение при отсутствии. Ничего больше.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY1MTMwNDAwMV19
-->
