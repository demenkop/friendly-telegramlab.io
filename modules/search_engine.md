---
title: Google-поиск
layout: default
parent: Modules
---

# {{ page.title }}

## Commands

- **Google Поиск [BETA]**
[Syntax: `google [запрос]`]

  Ищет в Google ваш запрос, возвращая пару первых результатов прямо в чат. Намного лучше, чем открыть браузер для этого.

Поддержка новых поисковых движков будет добавлена в обновлениях.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc5MzYyMDkzMl19
-->
